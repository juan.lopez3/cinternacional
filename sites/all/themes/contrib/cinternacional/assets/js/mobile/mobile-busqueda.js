(function($, window, undefined) {
    
    //pagina de busqueda
    var wraper 					= $('.page-buscar').find('.dos-columnas-content'),
    	nueva_ubicacion_filtro 	= wraper.find('.pane-cuci-search-ci-sort'),
    	filtro 					= wraper.find('.region-right'),
    	markup_filtro 			= filtro.html();

    //se crea contenedor para poner el filtro
   	nueva_ubicacion_filtro.prepend('<div class="filtrar-por"><div class="label"></div><div class="region-right"></div></div>');

   	var filtrar_por = $('.filtrar-por');

   	//ponemos el contenido
   	filtrar_por.find('.region-right').append(markup_filtro); 

   	//borramos el markup del filtro para desktop
   	filtro.remove();

   	//abrir y cerrar filtro
   	wraper.on('click', '.filtrar-por > .label', function() {
   		filtrar_por.find('.region-right').toggleClass('active');
   		console.log('click');
   	});

})(jQuery, window);