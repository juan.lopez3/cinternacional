(function($, window, undefined) {
    $(document).ready(function(){

        //sticky elements
        sticky_menu('.menu-quienes-somos');
        sticky_menu('.page-node.node-type-noticias .redes_sociales');
        sticky_menu('.container_bloque_descarga_archivo'); //side bar interna producción

    }); //document ready

    //sticky menu function
    function sticky_menu(selector) {
        var elm = $(selector);

        if (!elm.length) { return; }

        var ElmTop    = elm.offset().top,
            ElmHeight = elm.height(),
            ProdRel = $('.producciones-relacionadas');

        var stickyNav = function(){  
            var scrollTop = $(window).scrollTop(),
                FooterTop = $('.footer-wrapper').offset().top,
                bp_footer,
                topNegativo;

                //verificamos si es la interna de una produccion
                if (ProdRel.length > 0) {
                    var ProdRelAlto = ProdRel.height();
                    //actualizamos variable    
                    bp_footer = FooterTop - ProdRelAlto - ElmHeight - 30; //breakpoint cuando se acerca al footer

                    topNegativo = bp_footer - scrollTop; //simula que el elemento sticky se pega al footer

                    if (scrollTop >= bp_footer) {
                        elm.css({ top: topNegativo });
                    } else {
                        elm.css({ top: 0 });
                    }

                } else {
                    bp_footer = FooterTop - ElmHeight - 60; //breakpoint cuando se acerca al footer

                    topNegativo = bp_footer - scrollTop; //simula que el elemento sticky se pega al footer

                    if (scrollTop >= bp_footer) {
                        elm.css({ top: topNegativo });
                    } else {
                        elm.css({ top: 0 });
                    }
                }

            if (scrollTop > ElmTop) {   
                elm.addClass('sticky');
            } else {  
                elm.removeClass('sticky');
            }
        };
          
        stickyNav();
          
        $(window).scroll(function() {  
            stickyNav();
        });
    }
})(jQuery, window);