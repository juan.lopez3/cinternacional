(function($, window, undefined) {
    $(window).load(function(){
        /*Despliegue footer*/
        $(".btn-up").click(despliegueFooter);
        /*Tabs catalogo listado - cuadricula*/
        $("#cuadricula").on('click', function(){
            var vcontent = $(".pane-catalogo-panel-pane-1");
            vcontent.removeClass("cuadricula");
            vcontent.removeClass("listado");
            vcontent.addClass("cuadricula");

            $("#listado").removeClass("active");
            $(this).addClass("active");
        });
        $("#listado").on('click', function(){
            var vcontent = $(".pane-catalogo-panel-pane-1");
            vcontent.removeClass("listado");
            vcontent.removeClass("cuadricula");
            vcontent.addClass("listado");

            $("#cuadricula").removeClass("active");
            $(this).addClass("active");
        });
    });

    function despliegueFooter(){
			$(this).toggleClass("open");
			$(".group-footer").slideToggle("slow");
		}
})(jQuery, window);