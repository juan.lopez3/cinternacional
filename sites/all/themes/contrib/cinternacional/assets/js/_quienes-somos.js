(function($, window, undefined) {
    $(document).ready(function(){
    	
    	var AboutMenu = $('ul.menu-quienes-somos'),
            AboutMenuItem = AboutMenu.find('li'),
    		secciones = {
    			//main content
    			0: '.node-page > .group-content > .field-name-title',
    			//ventas
    			1: '.node-page > .group-content .view-display-id-attachment_1',
    			//coproduccion
    			2: '.node-page > .group-content .view-display-id-attachment_1 .view-footer',
    			//equipo
    			3: '.node-page > .group-content .view-display-id-attachment_2'
    		};

		AboutMenuItem.on('click', function(e) {

        	e.preventDefault();

        	var name = $(this).index();
        	var selector = secciones[name];
        	goToByScroll(selector);

        });

        //Poner sección activa
        $(window).on("scroll",function(){ 
            seccionActiva();
        });

    }); //document ready


    //en click va al elemento
    function goToByScroll(item){
        // Scroll
        $('html,body').animate({
            scrollTop: $(item).offset().top - 100

        }, 'slow');
    }

    function seccionActiva(){
        var y = $(window).scrollTop() + 250,//Medida desde el top al hacer scroll
            elm_menu_quienes = $(".menu-quienes-somos"),
            _secciones = {
                //main content
                0: '.node-page > .group-content > .field-name-title',
                //ventas
                1: '.node-page > .group-content .view-display-id-attachment_1',
                //coproduccion
                2: '.node-page > .group-content .view-display-id-attachment_1 .view-footer',
                //equipo
                3: '.node-page > .group-content .view-display-id-attachment_2'
            };

        if (elm_menu_quienes.length === 0) { return; } //se verifica que el elemento exista

        elm_menu_quienes.find('.active').removeClass('active');
        if( y < $(_secciones[1]).offset().top){
            elm_menu_quienes.find('li:eq(0)').addClass('active');
        }
        else if(y >= $(_secciones[1]).offset().top && y < $(_secciones[2]).offset().top){
            elm_menu_quienes.find('li:eq(1)').addClass('active');
        }
        else if(y >= $(_secciones[2]).offset().top && y < $(_secciones[3]).offset().top){
            elm_menu_quienes.find('li:eq(2)').addClass('active');
        }
        else if(y >= $(_secciones[3]).offset().top){
            elm_menu_quienes.find('li:eq(3)').addClass('active');
        }
    }

})(jQuery, window);