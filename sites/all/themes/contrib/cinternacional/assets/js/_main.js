(function($, window, undefined) {

    $(document).ready(function(){
        
        //variable global ruta al directorio: objeto de Drupal
        ruta = Drupal.settings.basePath + Drupal.settings.theme.path;

        //conditional loading
        yepnope({
            test: jQuery.browser.mobile,
            yep: [
                ruta + '/assets/js/mobile.js', 
                ruta + '/assets/js/mobile-menu.js'
            ], //si es un dispositivo movil
            nope: ruta + '/assets/js/desktop.js' // de lo contrario esto
        });

    });
})(jQuery, window);