(function($, window, undefined) {

    //definimos variable global
    var Produccion = {};

    $(document).ready(function(){

    	Produccion.anchoVentana 	= $(window).width();
        Produccion.menuFooter       = $('.container_bloque_descarga_archivo');
		Produccion.sinopsis 		= $('.producciones-sinopsis');
		Produccion.sinopsisBody 	= Produccion.sinopsis.find('.field-name-body');
		Produccion.personajes 		= $('.producciones-personajes').find('.field-name-field-descripcion');
		Produccion.openCont 		= '<div class="open-cont" data-active="false"></div>';
		Produccion.ficha 			= $('.producciones-ficha');
		Produccion.fichaLabel 		= Produccion.ficha.find('.field-label');

        $imagenPrincipal            = $('.producciones-header').find('.field-name-field-imagen img');

            //imagen principal animacion
            $imagenPrincipal.on('load',function(){
                $(this).addClass('animate-img');
            });

            // Si la imagen esta cacheada en el navegador lo detectamos y forzamos un evento load
            if ($imagenPrincipal.length > 0) { //se valida que la imagen exista
                if ($imagenPrincipal[0].complete){
                    $imagenPrincipal.load();
                }
            }

    	//ficha tecnica
    	Produccion.fichaLabel.attr('data-active', 'false');

    	Produccion.fichaLabel.on('click', function() {
			var _status = $(this).data('active');

			if (_status === false) {
				Produccion.ficha.addClass('active');
				$(this).data('active', true);
			} else {
				Produccion.ficha.removeClass('active');
				$(this).data('active', false);
			}
			
		});

    	$(window).resize(function() {
    		Produccion.anchoVentana = $(window).width();
    		if (Produccion.anchoVentana > 768) { //desktop

                //crear custom scroll
	    		crear_custom_scroll();

	    	} else { //moviles

                //destruye el scroll personalizado
	    		destruir_custom_scroll();
	    	}
    	});

    	if (Produccion.anchoVentana > 768) {

            //crear custom scroll
            crear_custom_scroll();

        } else {
    		//agregamos data para desplegar contenedor en moviles
    		Produccion.sinopsis.find('.group-mostrar-info').attr('data-active', 'false');

    		Produccion.sinopsis.find('.group-mostrar-info').on('click', function() {
    			var _status = $(this).data('active');

    			if (_status === false) {
    				Produccion.sinopsis.addClass('active');
    				$(this).data('active', true);
    			} else {
    				Produccion.sinopsis.removeClass('active');
    				$(this).data('active', false);
    			}
    			
    		});

    		//var _vacio = $('#flexslider-4').find('.group-left > div').index();
    		$('#flexslider-4').find('.group-left').each(function() {
    			var _checkImagen = $(this).find('div').index();
    			if (_checkImagen === -1 ) { $(this).next('.group-right').css('width', '100%'); }
    		});

            //menu footer
            menu_footer_producciones();
    	}

        /*
            Funcionalidad Galerias
        */
        //variables
        var gCaption = $('.group-galeria').find('.flex-caption');

        //ocultar descripcción
        gCaption.append('<div class="cerrar-flex-caption">x</div>');

        gCaption.find('.cerrar-flex-caption').on('click', function() {
            this.closest('.flex-caption').remove();
        });

        //play y pausa
        if ($(".group-galeria .flex-pauseplay").length) {
            //boton de zoom en galeria
            $(".group-galeria .flex-pauseplay").append("<div class='zoom'>zoom</div>");
            //popup de zoom 
            var markup_zoom = "<div class='overlay-zoom'></div>";
            $("body").append(markup_zoom);
        }
        //zoom de galeria
        $(".group-galeria .zoom").on("click", function(e){
            e.preventDefault();
            $(".producciones-galeria .flexslider").toggleClass("zoom-flexslider");
            $(".overlay-zoom").toggleClass("active");
            $('.producciones-galeria .flexslider').resize();
        });

	}); //document ready

    function menu_footer_producciones() {

        if (Produccion.menuFooter.length === 0) { return; }

        Produccion.sinopsisTop = Produccion.sinopsis.offset().top; //calculamos a cuantos pixeles esta del top del elem
        Produccion.altoSeccion = Produccion.sinopsis.outerHeight();
        Produccion.breakPoint = Produccion.sinopsisTop - Produccion.altoSeccion;

        var mostrar_menu = function() {

            Produccion.scrollTop = $(document).scrollTop();

            if ( Produccion.scrollTop >= Produccion.breakPoint ) {
                Produccion.menuFooter.addClass('active');
            } else {
                Produccion.menuFooter.removeClass('active');
            }

        };

        $(window).scroll(function() {  
            mostrar_menu();
        });

    }

    function crear_custom_scroll() {
        Produccion.sinopsisBody.perfectScrollbar({ suppressScrollX: true, maxScrollbarLength: 120 });
        Produccion.personajes.perfectScrollbar({ suppressScrollX: true, maxScrollbarLength: 120 });
    }

    function destruir_custom_scroll() {
        Produccion.sinopsisBody.perfectScrollbar('destroy');
        Produccion.personajes.perfectScrollbar('destroy');
    }

})(jQuery, window);