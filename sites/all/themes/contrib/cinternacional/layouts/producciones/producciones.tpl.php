<?php
/**
 * @file
 * layout producciones.
 */
?>

<!-- contenedor header -->
<div style="<?php print $color1; ?>" class="producciones-header producciones panel-display" <?php if (!empty($css_id)): print "id=\"$css_id\""; endif; ?>>
  <div class="region">
    <?php print $content['prod_header']; ?>
  </div>
</div>

<!-- contenedor sinopsis -->
<div style="<?php print $color2; ?>" class="producciones-sinopsis producciones panel-display" <?php if (!empty($css_id)): print "id=\"$css_id\""; endif; ?>>
  <div class="region">
    <?php print $content['prod_sinopsis']; ?>
  </div>
</div>

<!-- contenedor personajes -->
<div style="<?php print $color1; ?>" class="producciones-personajes producciones panel-display" <?php if (!empty($css_id)): print "id=\"$css_id\""; endif; ?>>
  <div class="region">
    <?php print $content['prod_personajes']; ?>
  </div>
</div>

<!-- contenedor galeria -->
<div style="<?php print $color2; ?>" class="producciones-galeria producciones panel-display" <?php if (!empty($css_id)): print "id=\"$css_id\""; endif; ?>>
  <div class="region">
    <?php print $content['prod_galeria']; ?>
  </div>
</div>

<!-- contenedor video -->
<div style="<?php print $color3; ?>" class="producciones-video producciones panel-display" <?php if (!empty($css_id)): print "id=\"$css_id\""; endif; ?>>
  <div class="region">
    <?php print $content['prod_video']; ?>
  </div>
</div>

<!-- contenedor ficha -->
<div style="<?php print $color1; ?>" class="producciones-ficha producciones panel-display" <?php if (!empty($css_id)): print "id=\"$css_id\""; endif; ?>>
  <div class="region">
    <?php print $content['prod_ficha']; ?>
  </div>
</div>

<!-- Producciones relacionadas -->
<div style="<?php print $color3; ?>" class="producciones-relacionadas producciones panel-display" <?php if (!empty($css_id)): print "id=\"$css_id\""; endif; ?>>
  <div class="region">
    <?php print $content['prod_relacionadas']; ?>
  </div>
</div>