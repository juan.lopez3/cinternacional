<?php
/**
 * @file
 * layout producciones.
 */
?>

<!-- contenedor header -->
<div class="home-header home panel-display" <?php if (!empty($css_id)): print "id=\"$css_id\""; endif; ?>>
  <div class="region">
    <?php print $content['home_header']; ?>
  </div>
</div>

<!-- contenedor prod_destacadas -->
<div class="home-prod_destacadas home panel-display" <?php if (!empty($css_id)): print "id=\"$css_id\""; endif; ?>>
  <div class="region">
    <?php print $content['home_prod_destacadas']; ?>
  </div>
</div>

<!-- contenedor catalogo -->
<div  class="home-catalogo home panel-display">
  <div class="region">
    <?php print $content['home_catalogo']; ?>
  </div>
</div>

<div class="notiRedes home panel-display">
  <div class="notiredesWrap">
    <!-- contenedor noticias -->
    <div style="<?php print $color2; ?>" class="home-noticias home panel-display" >
      <div class="region">
        <?php print $content['home_noticias']; ?>
      </div>
    </div>

    <!-- contenedor redes -->
    <div class="home-redes home panel-display" >
      <div class="region">
        <?php print $content['home_redes']; ?>
      </div>
    </div>
  </div>
</div>
<!-- contenedor banner -->
<div class="home-banner home panel-display" >
  <div class="region">
    <?php print $content['home_banner']; ?>
  </div>
</div>

<!-- contenedor ejecutivos -->
<div  class="home-ejecutivos home panel-display" >
  <div class="region">
    <?php print $content['home_ejecutivos']; ?>
  </div>
</div>
