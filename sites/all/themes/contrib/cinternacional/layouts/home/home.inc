<?php
/**
 * @file
 * Implementation of hook_panels_layouts
 */
function cinternacional_home_panels_layouts() {
  $items['home'] = array(
    'title'    => t('home'),
    'category' => t('home'),
    'icon'     => 'home.png',
    'theme'    => 'home',
    'admin css' => 'home.admin.css',
    'regions' => array(
      'home_header' => t('header'),
      'home_prod_destacadas' => t('p_destacadas'),
      'home_catalogo' => t('catalogo'),
      'home_noticias' => t('noticias'),
      'home_redes' => t('redes'),
      'home_banner' => t('banner'),
      'home_ejecutivos' => t('ejecutivos')
    )
  );
  return $items;
}