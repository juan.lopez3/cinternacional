<!--.page -->
<!-- proximamente esto va para un modulo -->
<div style="z-index:99999;position:absolute;top:50px;left:0;background:#000;color:#fff;font-size:10px; opacity:0.5;" 
     id="mocion_device_debug">
  <div>current: <?php print MocionDetectDevice::device() ?></div>
  <div>
    <a href="<?php print url(NULL, array('absolute' => TRUE, 'query' => array("device" => "pc"))) ?>">PC</a>
    <a href="<?php print url(NULL, array('absolute' => TRUE, 'query' => array("device" => "handheld"))) ?>">handheld</a>
    <a href="<?php print url(NULL, array('absolute' => TRUE, 'query' => array("device" => ""))) ?>">Automatic</a>
  </div>
</div>

<?php 
global $language ;
?>

<div role="document" class="page">

  <?php if($color_seccion_barra) :?>
    <div class='color_seccion_barra' style="background-color: <?php print $color_seccion_barra; ?>;" >
    </div>
  <?php endif; ?>

  <!--.l-header -->
  <div class="degradado"></div>

  <header role="banner" class="l-header <?php print (MocionDetectDevice::isHandheld()) ? "mobile" : ""  ?>"> 

    <a href="<?php                     
    
    $lang_name = $language->language ;                    
    print base_path().$lang_name ?>" class="logo">
      <img src="<?php global $t;
    print $t ?>/images/header/logo-azul.png" alt="Logo"></a>


<?php if (MocionDetectDevice::isHandheld()): ?>

      <div class="header-top mobile">
        <?php print render($page['header']); ?>
        <div class='search-form'>
          <?php $search_form = block_load('apachesolr_panels', 'search_form');
            if (isset($search_form->bid)) {
              $block = _block_get_renderable_array(_block_render_blocks(array($search_form)));
              print render($block);
            }//end if
          ?>
        </div>
      </div>
      
      <div class="header-bottom">
        <div class="menuMobile">
          <div class="menu-wrap">
            <nav class="menu">
              <div class="icon-list">
                <?php
//                $menu = menu_navigation_links('menu-menu-footer');
//                print theme('links__menu_menu_footer', array('links' => $menu));
                
                  $menup = menu_navigation_links('main-menu');                      
                  foreach ($menup as $key => $menulink){                      
                    $menup[$key]['title'] = t($menulink['title']);
                  }                      
                  print theme('links__main_menu', array('links' => $menup));  

                  $menu = menu_navigation_links('menu-menu-principal-footer');
                  print theme('links__menu_menu_principal_footer', array('links' => $menu));
                ?>
              </div>
            </nav>
            <div class="redes-top mobile">
              <ul class="redes">
                <li class="facebook"><a href=" https://www.facebook.com/CaracolTvInter" target="_blank"></a></li>
                <li class="twitter"><a href="https://twitter.com/caracoltvintl" target="_blank"></a></li>
                <li class="gplus"><a href="https://plus.google.com/116180247049728280275" target="_blank"></a></li>
              </ul>
            </div>
          </div>
        </div>
        <div class='titulo-pagina'><?php print $titulo_pagina; ?></div>
        <button class="menu-button" id="open-button">Open Menu</button>
      </div>

    <?php endif ?>
<?php if (!MocionDetectDevice::isHandheld()): ?>

      <div class="header-right">
        <div class="header-top">

          <div class="top-right">
            <div class='search-form'>
              <?php
              $search_form = block_load('apachesolr_panels', 'search_form');
              if (isset($search_form->bid)) {
                $block = _block_get_renderable_array(_block_render_blocks(array($search_form)));
                print render($block);
              }
              ?>
            </div>
            <div class="redes-top">
              <ul class="redes">
                <li class="facebook"><a href=" https://www.facebook.com/CaracolTvInter" target="_blank"></a></li>
                <li class="twitter"><a href="https://twitter.com/caracoltvintl" target="_blank"></a></li>
                <li class="gplus"><a href="https://plus.google.com/116180247049728280275" target="_blank"></a></li>
              </ul>
            </div>
            <div class="header-adm">
              <?php print render($page['header']); ?>
            </div>
            <div class="zona-usuarios">
              
             <?php
             
               $block = module_invoke('zona_usuario', 'block_view', 'block_zona_usuario');
               print render($block['content']);
             
             ?>
              

            </div>
          </div><!-- top right -->
          <div class="top-left">

            <?php            
            if($language->language == 'es') 
              $u = 'quienes-somos';
            else
              $u = 'about-us';
            ?>
              
            <?php print t('<a href="@url">About Us</a>', array('@url' => url($u))); ?>
            <?php print t('<a href="@url">News</a>', array('@url' => url('noticias'))); ?>             
         
          </div><!-- top left -->
        </div>
        <div class="main-menu">
  <?php print render($page['menu']); ?>
        </div>
      </div>
<?php endif; //endif no es movil  ?>
  </header>

<?php if (!empty($page['featured'])): ?>
    <!--.featured -->
    <section class="l-featured row">
      <div class="large-12 columns">
  <?php print render($page['featured']); ?>
      </div>
    </section>
    <!--/.l-featured -->
  <?php endif; ?>

    <div id="contenedor-mensaje"> 
      <?php if ($messages && !$cinternacional_messages_modal): ?>
        <!--.l-messages -->
        <section class="l-messages row">
          <div class="large-12 columns">
            <?php if ($messages): print $messages; endif; ?>
          </div>
        </section>
        <!--/.l-messages -->
      <?php endif; ?>
    </div>

<?php if (!empty($page['help'])): ?>
    <!--.l-help -->
    <section class="l-help row">
      <div class="large-12 columns">
  <?php print render($page['help']); ?>
      </div>
    </section>
    <!--/.l-help -->
<?php endif; ?>

  <main role="main" class="row l-main">
    <div id="loading"></div>
    <div class="<?php print $main_grid; ?> main columns">
        <?php if (!empty($page['highlighted'])): ?>
        <div class="highlight panel callout">
        <?php print render($page['highlighted']); ?>
        </div>
<?php endif; ?>

      <a id="main-content"></a>

      <?php if ($breadcrumb): /* print $breadcrumb; */ endif; ?>

      <?php if ($title && !$is_front): ?>
        <?php $type = arg(0);
        if ($type != 'node') {
          ?>
          <?php print render($title_prefix); ?>
          <h1 id="page-title" class="title"><?php print $title; ?></h1>
          <?php print render($title_suffix); ?>
        <?php } ?>
      <?php endif; ?>

      <?php if (!empty($tabs)): ?>
        <?php print render($tabs); ?>
        <?php if (!empty($tabs2)): print render($tabs2);
        endif; ?>
        <?php endif; ?>

      <?php if ($action_links): ?>
        <ul class="action-links">
        <?php print render($action_links); ?>
        </ul>
<?php endif; ?>

    <?php print render($page['content']); ?>
    </div>
    <!--/.l-main region -->

    <?php if (!empty($page['sidebar_first'])): ?>
      <aside role="complementary" class="<?php print $sidebar_first_grid; ?> l-sidebar-first columns sidebar">
      <?php print render($page['sidebar_first']); ?>
      </aside>
      <?php endif; ?>

    <?php if (!empty($page['sidebar_second'])): ?>
      <aside role="complementary" class="<?php print $sidebar_sec_grid; ?> l-sidebar-second columns sidebar">
  <?php print render($page['sidebar_second']); ?>
      </aside>
<?php endif; ?>
  </main>
  <!--/.l-main-->
  <div class="footer-wrapper">
    <div class="contact-footer">

          <p><?php print t('Do you want more information about any of our productions ?') ?></p>
          <?php print t('<a href="@url">Contact Us</a>', array('@url' => url('contact'))); ?>
      
    </div>

<?php if (!MocionDetectDevice::isHandheld()): ?>

      <footer class="footer" role="contentinfo">
        <div class="row">
          <div class="first">
            <div id="footer-first" class="section-wrapper">
              <section class="block block-block block-block-7">
                <div class="footer-site">
                  <div class="top-footer">
                    <a href="<?php                     
                    $lang_name = $language->language ;                    
                    print base_path().$lang_name ?>" class="logo">
                      <img src="<?php global $t;
                    print $t ?>/images/header/logo.png" alt="Logo"></a>

                    <div class="menuFooter">
                      <?php

                      $menup = menu_navigation_links('main-menu');                      
                      foreach ($menup as $key => $menulink){                      
                        $menup[$key]['title'] = t($menulink['title']);
                      }                      
                      print theme('links__main_menu', array('links' => $menup));  

                      $menu = menu_navigation_links('menu-menu-principal-footer');
                      print theme('links__menu_menu_principal_footer', array('links' => $menu));
                      ?>
                    </div>

                    <div class="redes-footer">
                      <ul class="redes">
                        <li class="facebook"><a href=" https://www.facebook.com/CaracolTvInter" target="_blank"></a></li>
                        <li class="twitter"><a href="https://twitter.com/caracoltvintl" target="_blank"></a></li>
                        <li class="gplus"><a href="https://plus.google.com/116180247049728280275" target="_blank"></a></li>
                      </ul>
                    </div>
                    <div class="btn-up">&nbsp;</div>
                  </div>

                  <div class="group-footer" style="overflow: hidden; display: block;">

                      <div class="middle-footer">
                        <div class="row-menu">
                          <h2><?php print t('Programs'); ?></h2>
                          <ul class="lista">
                            <li><a href="http://www.caracoltv.com/programacion" target="_blank" ><?php print t('Programming'); ?></a></li>
                            <li><a href="http://www.caracoltv.com/canaldevideo/programas" target="_blank" ><?php print t('Video Channel'); ?></a></li>
                            <li><a href="http://www.caracoltv.com/producciones" target="_blank" ><?php print t('Productions'); ?></a></li>
                            <li><a href="http://www.caracoltv.com/contact/programas" target="_blank" ><?php print t('Contact Us'); ?></a></li>
                          </ul>
                        </div>
                        <div class="row-menu">
                          <h2><?php print t('Noticias Caracol'); ?></h2>
                          <ul class="lista">
                            <li><a href="http://www.noticiascaracol.com/canal-de-video" ><?php print t('Video Channel'); ?></a></li>
                            <li><a href="http://www.noticiascaracol.com/" ><?php print t('Colombia'); ?></a></li>
                            <li><a href="http://www.noticiascaracol.com/mundo" ><?php print t('World'); ?></a></li>
                            <li><a href="http://www.noticiascaracol.com/entretenimiento" ><?php print t('Entertainment'); ?></a></li>
                            <li><a href="http://www.noticiascaracol.com/deportes" ><?php print t('Sports'); ?></a></li>
                            <li><a href="http://www.noticiascaracol.com/informativos" ><?php print t('Informative'); ?></a></li>
                            <li><a href="http://www.noticiascaracol.com/especiales" ><?php print t('Specials'); ?></a></li>
                            <li><a href="http://www.noticiascaracol.com/codigo-caracol" ><?php print t('Código caracol'); ?></a></li>
                            <li><a href="http://www.noticiascaracol.com/gente-que-le-pone-el-alma" ><?php print t('Gente que le pone el alma'); ?></a></li>
                            <li><a href="http://www.noticiascaracol.com/lo-mas-trinado" ><?php print t('#Lo más trinado'); ?></a></li>
                            <li><a href="http://www.noticiascaracol.com/salud" ><?php print t('Health'); ?></a></li>
                          </ul>
                        </div>
                        <div class="row-menu">
                          <h2><?php print t('Gol Caracol'); ?></h2>
                          <ul class="lista">
                            <li><a href="http://www.golcaracol.com" target="_blank"><?php print t('Home'); ?></a></li>
                            <li><a href="http://www.golcaracol.com/canaldevideo/golcaracol" target="_blank"><?php print t('Video Channel'); ?></a></li>
                            <li><a href="http://www.golcaracol.com/podcast" target="_blank"><?php print t('Podcast'); ?></a></li>
                            <li><a href="http://www.golcaracol.com/seleccioncolombia" target="_blank"><?php print t('Selección Colombia'); ?></a></li>
                            <li><a href="http://www.golcaracol.com/torneosyligas" target="_blank"><?php print t('Tournaments and Leagues'); ?></a></li>
                            <li><a href="http://www.golcaracol.com/contact/golcaracol" target="_blank"><?php print t('Contact Us'); ?></a></li>
                          </ul>
                        </div>
                        <div class="row-menu">
                          <h2><?php print t('Caracol Play'); ?></h2>
                          <ul>
                            <li><a href="http://www.caracolplay.com/suscripcion/catalogo/telenovelas" target="_blank" ><?php print t('Soap opera'); ?></a></li>
                            <li><a href="http://www.caracolplay.com/suscripcion/catalogo/series" target="_blank" ><?php print t('Series'); ?></a></li>
                            <li><a href="http://www.caracolplay.com/suscripcion/catalogo/peliculas" target="_blank" ><?php print t('Movies'); ?></a></li>
                            <li><a href="http://www.caracolplay.com/suscripcion/catalogo/noticias" target="_blank" ><?php print t('News'); ?></a></li>
                          </ul>
                        </div>
                        <div class="row-menu">
                          <h2><?php print t('Our network of portals'); ?></h2>
                          <ul class="lista">
                            <li><a href="http://www.elespectador.com/" target="_blank" >Elespectador.com</a></li>
                            <li><a href="http://www.cromos.com.co" target="_blank" >Cromos.com.co</a></li>
                            <li><a href="http://www.shock.co/" target="_blank" >Shock.co</a></li>
                            <li><a href="http://www.caracolplay.com/" target="_blank" >Caracolplay.com</a></li>
                            <li><a href="http://www.bluradio.com" target="_blank" >Bluradio.com</a></li>
                            <li><a href="http://www.dataifx.com" target="_blank" >Dataifx.com</a></li>
                            <li><a href="http://www.cinecolombia.com" target="_blank" >Cinecolombia.com</a></li>
                            <li><a href="http://www.icck.net.co" target="_blank" >Paute con nosotros</a></li>
                            <li><a href="http://www.primerafila.com.co/" target="_blank" >Primerafila.com.co</a></li>
                            <li><a href="http://www.teatromayor.org/" target="_blank" >Teatromayor.org</a></li>
                          </ul>
                        </div>
                      </div>
                      <div class="bottom-footer">
                        <div class="botom-row">
                          <h2><?php print t('Corporate services:'); ?></h2>
                          <ul class="listado">
                            <li><a href="http://caracoltv.com/contact/programas" target="_blank"><?php print t('Service viewers'); ?></a></li>
                            <li><a href="http://caracoltv.com/defensordeltelevidente" target="_blank"><?php print t('Defensor viewer'); ?></a></li>
                            <li><a href="http://apps.caracoltv.com/sqrd/main.aspx?in=3" target="_blank"><?php print t('Contributors'); ?></a></li>
                            <li><a href="http://caracoltv.com/responsabilidadsocial" target="_blank"><?php print t('Social responsibility'); ?></a></li>
                            <li><a href="" target="_blank" ><?php print t('Corporate'); ?></a></li>
                            <li><a href="http://extranet.canalcaracol.com/intermedia/index.html" target="_blank"><?php print t('Portal of customers and suppliers'); ?></a></li>
                            <li><a href="http://caracoltv.com/serviciosdeproduccion" target="_blank"><?php print t('Production Services'); ?></a></li>
                            <li><a href="http://caracoltv.com/codigodelbuengobierno" target="_blank"><?php print t('Code of good governance'); ?></a></li>
                            <li><a href="http://www.caracoltv.com/contact/programas" target="_blank"><?php print t('Contact Us'); ?></a></li>
                          </ul>
                        </div>
                        <div class="botom-row">
                          <h2><?php print t('Associated media:'); ?></h2>
                          <ul class="listado">
                            <li><a href="http://www.bbc.co.uk" target="_blank">BBC</a></li>
                            <li><a href="http://www.unitel.tv/site/index.php" target="_blank">UNITEL</a></li>
                            <li><a href="http://www.atv.com.pe" target="_blank">ATV</a></li>
                            <li><a href="http://www.rctv.net" target="_blank">RCTV.NET</a></li>
                            <li><a href="http://www.montecarlotv.com.uy" target="_blank">Monte Carlo TV</a></li>
                            <li><a href="http://www.telefe.com" target="_blank">Telefe</a></li>
                            <li><a href="http://www.mega.cl" target="_blank">MEGA</a></li>
                            <li><a href="http://www.ecuavisa.com" target="_blank">Ecuavisa</a></li>
                            <li><a href="http://www.tvn-2.com" target="_blank">TVN-2.com</a></li>
                          </ul>
                        </div>
                        <div class="botom-row">
                          <h2><?php print t('Legal:'); ?></h2>
                          <ul class="listado">
                            <li><a href="http://static.canalcaracol.com/movil/Acuerdo2010RCNCARACOL.pdf" target="_blank"><?php print t('Conditions of access TV Network'); ?></a></li>
                            <li><a href="http://www.caracoltv.com/asamblea-de-accionistas-2014" target="_blank"><?php print t('Information about the general assembly 2014'); ?></a></li>
                            <li><a href="http://www.caracoltv.com/asamblea-extraordinaria-de-accionistas-2014" target="_blank"><?php print t('Information about the extraordinary shareholders meeting 2014'); ?></a></li>
                            <li><a href="http://www.caracoltv.com/informacion-bolsa-de-valores" target="_blank"><?php print t('Corporate'); ?></a></li>
                            <li><a href="http://www.caracoltv.com/responsabilidadcorporativa" target="_blank"><?php print t('Corporate Responsibility'); ?></a></li>
                            <li><a href="http://static.canalcaracol.com/movil/POLITICAS-PRIVACIDAD-CARACOLTV.pdf" target="_blank"><?php print t('Policy Information Processing'); ?></a></li>
                          </ul>
                        </div>
                      </div>
                    </div>

                  <div class="pie">
                    <div class="copyright">                     
                        <p class="terms-conditions"><?php print t('Use of this website constitutes acceptance of the'); ?> 
                          <a href="<?php print base_path().$lang_name ?>/terminos-y-condiciones" ><?php print t('Terms and Conditions'); ?></a> <?php print t('and'); ?> 
                          <a href="http://apps.caracoltv.com/f/POLITICAS%20DE%20TRATAMIENTO.pdf" target="_blank" ><?php print t('Treatment Policy'); ?></a>
                             <?php print t(' Caracol Televisión SA All Rights Reserved D.R.A. Prohibited its total or partial reproduction and translation into any language without written permission is prohibited .'); ?>
                          <br> <?php print t('All rights reserved 2015'); ?></p>                      
                      </div>
                  </div>
                </div> 
              </section>            
            </div> <!--#footer-first -->
          </div>
        </div>
      </footer>

    <?php endif; //endif no es movil ?>

<!--   <a href="<?php #print base_path()?>" class="logoFooterMobile">
      <img src="<?php #global $t; print $t ?>/images/header/logo.png" alt="Logo">
  </a> -->
  </div>

<?php if (MocionDetectDevice::isHandheld()): ?>
    <div class="menuFooter mobile">
      <a href="<?php $lang_name = $language->language; print base_path().$lang_name ?>" class="logo">
        <img src="<?php global $t; print $t ?>/images/header/logo.png" alt="Logo">
      </a>
  <?php
      $menup = menu_navigation_links('main-menu');                      
      foreach ($menup as $key => $menulink){                      
        $menup[$key]['title'] = t($menulink['title']);
      }                      
      print theme('links__main_menu', array('links' => $menup));  

      $menu = menu_navigation_links('menu-menu-principal-footer');
      print theme('links__menu_menu_principal_footer', array('links' => $menu));
  ?>
    </div>
    <div class="redes-footer mobile">
      <ul class="redes">
        <li class="facebook"><a href=" https://www.facebook.com/CaracolTvInter" target="_blank"></a></li>
        <li class="twitter"><a href="https://twitter.com/caracoltvintl" target="_blank"></a></li>
        <li class="gplus"><a href="https://plus.google.com/116180247049728280275" target="_blank"></a></li>
      </ul>
    </div>
    <div class="copyright mobile">
              
        <p class="terms-conditions"><?php print t('Use of this website constitutes acceptance of the'); ?> 
        <a href="<?php print base_path().$lang_name ?>/terminos-y-condiciones" ><?php print t('Terms and Conditions'); ?></a> <?php print t('and'); ?> 
        <a href="http://apps.caracoltv.com/f/POLITICAS%20DE%20TRATAMIENTO.pdf" target="_blank" ><?php print t('Treatment Policy'); ?></a>
           <?php print t(' Caracol Televisión SA All Rights Reserved D.R.A. Prohibited its total or partial reproduction and translation into any language without written permission is prohibited .'); ?>
        <br> <?php print t('All rights reserved 2015'); ?></p>   
    </div>
<?php endif; //endif es movil ?>

  <!--/.footer-->
<?php if ($messages && $cinternacional_messages_modal): print $messages;
endif; ?>
  </div>
<!--/.page -->
