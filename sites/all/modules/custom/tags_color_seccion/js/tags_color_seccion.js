(function($) {

  $(document).ready(function() {
    
    //crear el div de la linea
    var $top_seccion  = $("<div id='top_seccion_rotador'></div>").prependTo("body");

    $("#rotador_producciones_destacadas_home").on( "after start", function() {
      cambiar_color_top_seccion(this)
    });

    function cambiar_color_top_seccion(elemento){
  
     var $color_seccion = $(elemento).find(".flex-active-slide .color-seccion");
     var color = $color_seccion.css("background-color");
     $top_seccion.css("background-color",color);

    }

  });
})(jQuery);