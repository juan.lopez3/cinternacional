<?php if (!MocionDetectDevice::isHandheld()): ?>
<div class="container_bloque_descarga_archivo">
  
  <div class="contactenos">
    <span class="icono_contactenos"></span>
    <p><?php print t("Interested in this production ?"); ?></p>
    <p><?php print t('<a href="@url">Contact Us</a>', array('@url' => url('contact'))); ?></p>
  </div>
  
  
  <?php if($ver_brochure == TRUE) :?>    
    <div class="brochure">
      <p>Brochure</p>
      <a href="<?php print $link_descargar_brochure; ?>">
        <span class="pdf">pdf</span></a>

      <span class="enviar-correo" rel="<?php print $link_enviar_brochure; ?>">enviar-correo</span>
      <div id="form-enviar-brocure" rel="<?php print $idnode; ?>" relfile="<?php print $idfile; ?>">
        <?php print $form; ?>
      </div>
    </div>
  <?php endif; ?>
  
  <?php if($ver_link_descargar_material == TRUE) :?>
    <div class="descargar_file">
      <a href="<?php print $link_descargar_material ?>"><span class="icon-descargar-file"></span>
        <p><?php print t("Download materials"); ?></p></a>
    </div>
  <?php endif; ?>
</div>

<?php endif; //endif no es movil ?>

<?php if (MocionDetectDevice::isHandheld()): ?>

<div class="container_bloque_descarga_archivo">
  <p><?php print t("Interested in this production ?"); ?></p>
  
  <div class="contactenos">
    <p><?php print t('<a href="@url">Contact Us</a>', array('@url' => url('contact'))); ?></p>
  </div>
  
  <div class="brochure">
    <span class="enviar-correo" rel="<?php print $link_enviar_brochure; ?>">Enviar brochure</span>
      <div id="form-enviar-brocure" rel="<?php print $idnode; ?>" relfile="<?php print $idfile; ?>">
        <?php print $form; ?>
      </div>
  </div>
  
</div>

<?php endif; ?>

