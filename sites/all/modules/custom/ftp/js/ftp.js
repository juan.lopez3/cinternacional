(function($) {

  $(document).ready(function(){

    var textos = Drupal.settings.ftp.textos.ftp;
    var lenguaje = Drupal.settings.ftp.lenguaje;

    $("#block-ftp-ftp-block .files-container a").live("click", function(e){
      e.preventDefault();

      if($(this).hasClass('folder')) {
        var path = $(this).attr("data-rel");

        $(".folders .folder a[data-rel='"+path+"'").click();
      }

    });

    //click en folders
    $(".folders li a.no-child").live("click", function(e){
      e.preventDefault();

      var elem = $(this);

      //contenedor de informacion de archivos
      var content = $("#block-ftp-ftp-block .right .files-container");

      elem.removeClass("no-child");
      elem.addClass("child");

      $(".folders li a").removeClass("active");
      $(this).addClass("active");

      $(".overlay").show();
      var path = $(this).attr("data-rel");

      var up_level = path;
      var index = up_level.lastIndexOf("/");
      if(index == -1) {
        //show_error("Error de directorio");
      }else{
        up_level = up_level.substring(0, index);
        $(".up-level a").attr("data-rel", up_level);
      }

      //url para folder
      $("#create-folder a").attr("data-rel", path);
      //url para subir archivo
      $("#upload-file a").attr("data-rel", path);
      $("#input_path").val(path);


      //filtros
      var f_filename = $("#file_name").attr("class");
      var f_size = $("#size").attr("class");
      var f_type = $("#type").attr("class");
      var f_modified = $("#modified").attr("class");

      var filtro = "text";
      var order = false; //false es ascendente
      if ($(".files-container table tr th.selected").length) {
        var identifier = $(".files-container table tr th.selected").attr("id");
        //asignamos a la variable filtro el valor del campo que usaremos para ordenar
        switch(identifier){
          case "file_name":
          filtro = "text";
          break;
          case "size":
          filtro = "size_org";
          break;
          case "type":
          filtro = "mime";
          break;
          case "modified":
          filtro = "modified_unix";
          break;
        }

        if ($(".files-container table tr th.selected").hasClass("desc")) {
          order = true; //true es descendente
        }

      }


      $.ajax({
        url: "ftp/get_list_files",
        type: "POST",
        dataType: 'json',
        data: {
          op: 'list_dirs',
          url: path,
        },
        success: function(folders) {

          $.ajax({
            url: "ftp/get_list_files",
            type: "POST",
            dataType: 'json',
            data: {
              op: 'list_files',
              url: path,
              field : filtro,
              order : order,
            },
            success: function(files) {

              content.empty();
              if($.isEmptyObject(files)) {
                //si el directorio esta vacio
                content.empty();
                var markup = "<table><tr>";
                markup += "<th id='file_name' class='"+f_filename+"'>"+textos.generales[lenguaje].nombre_archivo+"</th>"; 
                markup += "<th id='size' class='"+f_size+"'>"+textos.generales[lenguaje].peso+"</th>"; 
                markup += "<th id='type' class='"+f_type+"'>"+textos.generales[lenguaje].tipo+"</th>"; 
                markup += "<th id='modified' class='"+f_modified+"'>"+textos.generales[lenguaje].creado+"</th>"; 
                markup += "<th id='actions'>"+textos.generales[lenguaje].acciones+"</th>"; 
                markup += "</tr></table>";
                markup += "<div class='empty-message'>"+textos.generales[lenguaje].directorio_vacio+"</div>";
                content.append(markup);
              } else {

                //region izquierda o folders
                var markup = "";
                $.each( folders, function( i, val ) {

                  markup += "<li class='folder'><a href='#' data-rel='"+val.id+"' id='folder-"+i+"' class='no-child'>"+val.text;
                  if(val.new_files > 0) {
                    markup += "<span class='news'>"+val.new_files+"</span>";
                  }
                  markup += "</a></li>";

                });
                elem.parent("li").append("<ul class='folders'>"+markup+"</ul>");

                //region derecha o archivos


                var right = "<table><tr>";
                right += "<th id='file_name' class='"+f_filename+"'>"+textos.generales[lenguaje].nombre_archivo+"</th>"; 
                right += "<th id='size' class='"+f_size+"'>"+textos.generales[lenguaje].peso+"</th>"; 
                right += "<th id='type' class='"+f_type+"'>"+textos.generales[lenguaje].tipo+"</th>"; 
                right += "<th id='modified' class='"+f_modified+"'>"+textos.generales[lenguaje].creado+"</th>"; 
                right += "<th id='actions'>"+textos.generales[lenguaje].acciones+"</th>"; 
                right += "</tr>";
                $.each( files, function( i, val ) {

                  right += "<tr>";
                  right += "<td>"+ val.img +"<a href='#' data-rel='"+val.id+"' id='files-"+i+"' class='no-child "+val.clase+" "+val.cls+"'>"+val.text+"</a></td>";
                  if (val.size){
                   right += "<td><a href='#' data-rel='"+val.id+"' id='files-"+i+"' class='no-child "+val.clase+" "+val.cls+"'>"+val.size+"</a></td>";
                  } else {
                   right += "<td></td>";
                  }

                  right += "<td><a href='#' data-rel='"+val.id+"' id='files-"+i+"' class='no-child "+val.clase+" "+val.cls+"'>"+val.mime+"</a></td>";
                  right += "<td><a href='#' data-rel='"+val.id+"' id='files-"+i+"' class='no-child "+val.clase+" "+val.cls+"'>"+val.modified+"</a></td>";
                  if (val.cls == "file") {
                    right += "<td>";
                    if(val.clase == 'image') {
                      right += "<a id='ver' href='#' data-rel='"+val.id+"' id='files-"+i+"' class='no-child "+val.clase+" "+val.cls+"'>ver</a>";
                    }
                    right += "<a id='descargar' href='#' data-rel='"+val.id+"' id='files-"+i+"' class='no-child "+val.clase+" "+val.cls+"'>Descargar</a></td>";
                  } else {
                    right += "<td><a id='ver' href='#' data-rel='"+val.id+"' id='files-"+i+"' class='no-child "+val.clase+" "+val.cls+"'>ver</a></td>";
                  }
                  right += "</tr>";

                });
                content.append(right);

              }

            }
          });          

          $(".overlay").hide();
        }

      });

    }); // end click folders




    //handler click en folder que ya tiene hijos
    $(".folders li a.child, .files-container .folder, .up-level a").live("click", function(e){
      e.preventDefault();

      var elem = $(this);
      //contenedor de informacion de archivos
      var content = $("#block-ftp-ftp-block .right .files-container");

      elem.removeClass("no-child");
      elem.addClass("child");

      $(".folders li a").removeClass("active");

      $(".overlay").show();
      var path = $(this).attr("data-rel");
      $(".folders li a[data-rel='"+path+"']").addClass("active");

      //region derecha o archivos
      var up_level = path;
      var index = up_level.lastIndexOf("/");

      //path por defecto
      var default_path = Drupal.settings.ftp.root_path;      

      if(index == -1) {
        //show_error("Error de directorio");
      } else {
        up_level = up_level.substring(0, index);

        if(up_level.indexOf(default_path) > -1) {
          $(".up-level a").attr("data-rel", up_level);
        } else {
          $(".up-level a").attr("data-rel", default_path);
        }
      }

      //url para folder
      $("#create-folder a").attr("data-rel", path);
      //url para subir archivo
      $("#upload-file a").attr("data-rel", path);
      $("#input_path").val(path);

      //current path
      current_path = path;

      //filtros
      var f_filename = $("#file_name").attr("class");
      var f_size = $("#size").attr("class");
      var f_type = $("#type").attr("class");
      var f_modified = $("#modified").attr("class");

      var filtro = "text";
      var order = false; //false es ascendente
      if ($(".files-container table tr th.selected").length) {
        var identifier = $(".files-container table tr th.selected").attr("id");
        //asignamos a la variable filtro el valor del campo que usaremos para ordenar
        switch(identifier){
          case "file_name":
          filtro = "text";
          break;
          case "size":
          filtro = "size_org";
          break;
          case "type":
          filtro = "mime";
          break;
          case "modified":
          filtro = "modified_unix";
          break;
        }

        if ($(".files-container table tr th.selected").hasClass("desc")) {
          order = true; //true es descendente
        }

      }

      $.ajax({
        url: "ftp/get_list_files",
        type: "POST",
        dataType: 'json',
        data: {
          op: 'list_files',
          url: path,
          field : filtro,
          order : order,
        },
        success: function(files) {
          //console.log(files);
          content.empty();
          if($.isEmptyObject(files)) {
                //si el directorio esta vacio
                content.empty();
                var markup = "<table><tr>";
                markup += "<th id='file_name' class='"+f_filename+"'>"+textos.generales[lenguaje].nombre_archivo+"</th>"; 
                markup += "<th id='size' class='"+f_size+"'>"+textos.generales[lenguaje].peso+"</th>"; 
                markup += "<th id='type' class='"+f_type+"'>"+textos.generales[lenguaje].tipo+"</th>"; 
                markup += "<th id='modified' class='"+f_modified+"'>"+textos.generales[lenguaje].creado+"</th>"; 
                markup += "<th id='actions'>"+textos.generales[lenguaje].acciones+"</th>"; 
                markup += "</tr></table>";
                markup += "<div class='empty-message'>"+textos.generales[lenguaje].directorio_vacio+"</div>";
                content.append(markup);
              } else {

                var right = "<table><tr>";
                right += "<th id='file_name' class='"+f_filename+"'>"+textos.generales[lenguaje].nombre_archivo+"</th>"; 
                right += "<th id='size' class='"+f_size+"'>"+textos.generales[lenguaje].peso+"</th>"; 
                right += "<th id='type' class='"+f_type+"'>"+textos.generales[lenguaje].tipo+"</th>"; 
                right += "<th id='modified' class='"+f_modified+"'>"+textos.generales[lenguaje].creado+"</th>"; 
                right += "<th id='actions'>"+textos.generales[lenguaje].acciones+"</th>"; 
                right += "</tr>";
                $.each( files, function( i, val ) {

                  right += "<tr>";
                  right += "<td>"+ val.img +"<a href='#' data-rel='"+val.id+"' id='files-"+i+"' class='no-child "+val.clase+" "+val.cls+"'>"+val.text+"</a></td>";
                  if (val.size){
                   right += "<td><a href='#' data-rel='"+val.id+"' id='files-"+i+"' class='no-child "+val.clase+" "+val.cls+"'>"+val.size+"</a></td>";
                 } else {
                   right += "<td></td>";
                 }

                 right += "<td><a href='#' data-rel='"+val.id+"' id='files-"+i+"' class='no-child "+val.clase+" "+val.cls+"'>"+val.mime+"</a></td>";
                 right += "<td><a href='#' data-rel='"+val.id+"' id='files-"+i+"' class='no-child "+val.clase+" "+val.cls+"'>"+val.modified+"</a></td>";
                  if (val.cls == "file") {
                    right += "<td>";
                    if(val.clase == 'image') {
                      right += "<a id='ver' href='#' data-rel='"+val.id+"' id='files-"+i+"' class='no-child "+val.clase+" "+val.cls+"'>ver</a>";
                    }
                    right += "<a id='descargar' href='#' data-rel='"+val.id+"' id='files-"+i+"' class='no-child "+val.clase+" "+val.cls+"'>Descargar</a></td>";
                  } else {
                    right += "<td><a id='ver' href='#' data-rel='"+val.id+"' id='files-"+i+"' class='no-child "+val.clase+" "+val.cls+"'>ver</a></td>";
                  }
                 right += "</tr>";

               });
                content.append(right);

              }

               $(".overlay").hide();

            }
          });

    }); //end click subdirs


    //logica para filtros
    $(".files-container table tr th").live("click", function(e){
      var id = $(this).attr("id");
      console.log(id);

      if(id != "actions"){
        
        $(".files-container table tr th").removeClass("selected");
        $(this).addClass("selected");


        if ($(this).hasClass("asc")) {
          $(".files-container table tr th").removeClass("asc");
          $(this).addClass("desc");
        } else {
          if ($(this).hasClass("desc")) {
            $(".files-container table tr th").removeClass("desc");
            $(this).addClass("asc");
          } else {
            $(".files-container table tr th").removeClass("desc");
            $(".files-container table tr th").removeClass("asc");
            $(this).addClass("asc");
          }
        }

        refresh_file_list();

      }
      
    });//end click filters





    //handler popup imagenes
    $("#block-ftp-ftp-block #ver").live("click", function(e){
      e.preventDefault();

      if($(this).hasClass("file") && $(this).hasClass("image")) {

        var data = $(this).attr("data-rel");
        var name = "";
        var index = data.lastIndexOf("/");
        var base_path = Drupal.settings.ftp.base_path;

        if(index != -1) {
          name = data.substring(index+1);
        }

        var index2 = data.lastIndexOf("/sites");

        image_path = data.substring(index2 + 1, index);
        image_path += '/'+name; 

        var image = "<img src='"+base_path+""+image_path+"' alt="+name+" title="+name+" />";

        $("#pop-up .content").empty();
        $("#pop-up .content").append(image);
        $("#pop-up").show();

      }


    });

    //handler cerrar popup
    $("#pop-up .close").live("click", function() {
      $("#pop-up").hide();

      refresh_file_list();

    });


    //handler agregar folder
    $("#create-folder a").live("click", function(e) {
      e.preventDefault();
      var path = $(this).attr("data-rel");
      if(!path) {
        path = Drupal.settings.ftp.root_path;
      }
      var markup = "<div class='new-folder'>\
      <h4>"+textos.crear_directorio[lenguaje].titulo+"</h4>\
      <span>"+textos.crear_directorio[lenguaje].informacion+" <b>"+path+"</b></span>\
      <div class='form-container'>\
      <div class='error-container'></div>\
      <input type='text' class='name-folder' name='name-folder'/>\
      <div class='actions'>\
      <button id='aceptar'>"+textos.generales[lenguaje].boton_aceptar+"</button>\
      <button id='cancelar'>"+textos.generales[lenguaje].boton_cancelar+"</button>\
      </div>\
      </div>\
      </div>";

      $("#pop-up .content").empty();
      $("#pop-up .content").append(markup);
      $("#pop-up").show();

    });

    $(".new-folder #aceptar").live("click", function(e) {
      e.preventDefault();
      var name = $(".new-folder .name-folder").val();
      var path = $("#create-folder a").attr("data-rel");
      if(!path) {
        path = Drupal.settings.ftp.root_path;
      }

      if(name) {
        $.ajax({
          url: "ftp/get_list_files",
          type: "POST",
          data: {
            op: 'create_folder',
            url: path,
            name: name,
          },
          success: function(data) {
            if (data == 'exists') {
              $(".new-folder .error-container").empty();
              $(".new-folder .error-container").append("<span>"+textos.generales[lenguaje].carpeta_existente+"</span>");
            }
            else if (data == 'success'){
              refresh_file_list();
              $("#pop-up").hide();
            }
          }
        });
      } else {
        $(".new-folder .error-container").empty();
        $(".new-folder .error-container").append("<span>"+textos.generales[lenguaje].nombre_carpeta+"</span>");
      }

    });

    $("#pop-up #cancelar").live("click", function(e) {
      e.preventDefault();

      //refrescamos los archivos
      refresh_file_list();
      $("#pop-up").hide();

    });




    //handler agregar folder
    $("#upload-file a").live("click", function(e) {
      e.preventDefault();
      var path = $(this).attr("data-rel");
      if(!path) {
        path = Drupal.settings.ftp.root_path;
      }
      var markup = "<div class='upload-form'>\
      <h4>"+textos.subir_archivo[lenguaje].titulo+"</h4>\
      <span><b>"+path+"</b></span>\
      <div class='form-container'>\
      <form method='POST' enctype='multipart/form-data' id='upload-file-form'>\
      <div class='error-container'></div>\
      <div class='input-container'>\
        <label>Archivo: <input type='file' class='file-upload' name='uploaded-file[0]'/></label>\
      </div>\
      <button id='agregar-archivo'>"+textos.generales[lenguaje].boton_agregar_archivo+"</button>\
      <button id='aceptar-subir-archivo'>"+textos.generales[lenguaje].boton_subir_archivos+"</button>\
      <button id='cancelar'>"+textos.generales[lenguaje].boton_cancelar+"</button>\
      </form>\
      </div>\
      </div>";

      $("#pop-up .content").empty();
      $("#pop-up .content").append(markup);
      $("#pop-up").show();

    });

    /*Agregar  input file al subir archivos*/
    $("#agregar-archivo").live("click", function(e) {
      e.preventDefault();

      length_files = $(".input-container input").length;

      $(".input-container").append("<label>Archivo: <input type='file' class='file-upload' name='uploaded-file["+length_files+"]'/></label>");

    });


    /*Handler subir archivos*/
    $("#aceptar-subir-archivo").live("click", function(e) {
      e.preventDefault();
      var path = $("#upload-file a").attr("data-rel");
      if(!path) {
        path = Drupal.settings.ftp.root_path;
      }
      //var form = new FormData();
      //form.append('file', $("input[type=file]")[0].files[0]);
      var element = $("input[type=file]");
      var data = new FormData();
      for (i = 0; i < element.length; i++) {
        data.append('file' + i, element[i].files[0]);
      }
      //envio de archivo
      var xhr = new XMLHttpRequest;
      xhr.open('POST', "ftp/get_list_files?op=upload_file&url="+path, true);
      xhr.onreadystatechange = function(){
        var response = JSON.parse(xhr.response);

        var suc = eval(response.succeded);
        var fai = eval(response.failed);
        if (suc.length > 0) {
          var s = ""+textos.generales[lenguaje].archivos_cargados+"<br />";
          for (i = 0; i < suc.length; i++) {
            s = s + suc[i] + '<br />';
          }
        }
        if (fai.length > 0) {
          var s = ""+textos.generales[lenguaje].archivos_fallidos+"<br />";
          for (i = 0; i < fai.length; i++) {
            s = s + fai[i] + '<br />';
          }
        }
        //se limpia el formulario y se agrega el mensaje de respuesta
        element.val("");
        $(".upload-form .error-container").empty();
        $(".upload-form .error-container").append(s);


      }
      xhr.send(data);
    });


    $("#block-ftp-ftp-block #descargar").live("click", function(e){
      e.preventDefault();

      //ruta del archivo
      var file = $(this).attr("data-rel");
      //file = encodeURIComponent(file);
      //  console.log(file);

      var url = "ftp/get_list_files?op=download_file&file="+file;

      $.ajax({
        url: url,
        type: "GET",
        data: {
          op: 'download_file',
          file: file,
        },
        success: function(data) {
          $("#download_frame").remove();
          ifrm = document.createElement("IFRAME"); 
          ifrm.setAttribute("src", url); 
          ifrm.setAttribute("id", "download_frame"); 
          ifrm.style.width = 0+"px";
          ifrm.style.height = 0+"px";
          ifrm.style.display = "none";
          ifrm.style.visibility = "hidden";
          document.body.appendChild(ifrm);
        }
      });

    });

    $(".files-container table tr").live("click", function(e){
      $(".files-container table tr").removeClass("selected");
      $(this).addClass("selected");
    });

    /*popup renombrar*/
    $("#renombrar a").live("click", function(e){
      e.preventDefault();

      var path = $(".files-container table tr.selected td a").attr("data-rel");

      //si existe un item seleccionado
      if (path) {
        var markup = "<div class='rename-form'>\
        <h4>"+textos.renombrar[lenguaje].titulo+"</h4>\
        <span>"+textos.renombrar[lenguaje].informacion+"</span>\
        <span><b>"+path+"</b></span>\
        <div class='form-container'>\
        <form method='POST' enctype='multipart/form-data' id='rename-file-form'>\
        <div class='error-container'></div>\
        <div class='input-container'>\
          <input type='text' class='name-rename' name='name'/>\
        </div>\
        <button id='aceptar-renombrar'>"+textos.generales[lenguaje].boton_aceptar+"</button>\
        <button id='cancelar'>"+textos.generales[lenguaje].boton_cancelar+"</button>\
        </form>\
        </div>\
        </div>";

        $("#pop-up .content").empty();
        $("#pop-up .content").append(markup);
        $("#pop-up").show();
      }


    });

    $("#aceptar-renombrar").live("click", function(e){
      e.preventDefault();
      var path = $(".files-container table tr.selected td a").attr("data-rel");
      var name = $("#rename-file-form .name-rename").val();

      if (name) {
        $.ajax({
          url: "ftp/get_list_files",
          type: "POST",
          data: {
            op: 'rename',
            path: path,
            name: name,
          },
          success: function(data) {
            if(!data) {
              refresh_file_list();
              $("#pop-up").hide();
            } else {
              if(data.indexOf("failed") > 0) {
                $(".rename-file-form .error-container").append("<span>"+textos.generales[lenguaje].error_renombrar+"</span>");
              }
            }

          }
        });
      }
    });



    /*Eliminar archivo*/
    $("#eliminar a").live("click", function(e){
      e.preventDefault();

      var path = $(".files-container table tr.selected td a").attr("data-rel");

      //si existe un item seleccionado
      if (path) {
        var markup = "<div class='delete-form'>\
        <h4>"+textos.eliminar[lenguaje].titulo+"</h4>\
        <span>"+textos.eliminar[lenguaje].informacion+"</span>\
        <span>"+textos.eliminar[lenguaje].informacion2+"</span>\
        <div class='form-container'>\
        <form method='POST' enctype='multipart/form-data' id='rename-file-form'>\
        <div class='error-container'></div>\
        <div class='input-container'>\
          <div class='file'><b>"+path+"</b></div>\
        </div>\
        <button id='aceptar-eliminar'>"+textos.generales[lenguaje].boton_aceptar+"</button>\
        <button id='cancelar'>"+textos.generales[lenguaje].boton_cancelar+"</button>\
        </form>\
        </div>\
        </div>";

        $("#pop-up .content").empty();
        $("#pop-up .content").append(markup);
        $("#pop-up").show();
      }


    });

    $("#aceptar-eliminar").live("click", function(e){
      e.preventDefault();
      var path = $(".files-container table tr.selected td a").attr("data-rel");      
      if (path) {
        $.ajax({
          url: "ftp/get_list_files",
          type: "POST",
          data: {
            op: 'delete',
            nodes: path,
          },
          success: function(data) {
            refresh_file_list();
            $("#pop-up").hide();
          }
        });
      }
    });

    $("#block-ftp-ftp-block .ftp-menu").on('click', function() {
      $("#block-ftp-ftp-block .ftp-menu .links").toggleClass('active');
    });

    $("#block-ftp-ftp-block .zona-usuarios").on('click', function() {
      $("#block-ftp-ftp-block .zona-usuarios .item-list").toggleClass('active');
    });




  }); //end ready


$(document).keydown(function(e) {
    switch(e.which) {
        case 37: // left
          console.log("left");
        break;

        case 38: // up
          console.log("up");
          if ($(".files-container table tr.selected").length) {

          } 
        break;

        case 39: // right
          console.log("right");
        break;

        case 40: // down
          console.log("down");
        break;

        default: return; // exit this handler for other keys
    }
    e.preventDefault(); // prevent the default action (scroll / move caret)
});


/*
* Se listan nuevamente los archivos con respecto a la url actual
*/
function refresh_file_list() {

  var textos = Drupal.settings.ftp.textos.ftp;
  var lenguaje = Drupal.settings.ftp.lenguaje;

  var content = $("#block-ftp-ftp-block .right .files-container");

  var path = $("#input_path").val();
  
  //si no existe path, quiere decir que el directorio actual es la raiz
  if(!path) {
    path = Drupal.settings.ftp.root_path;
  }

  console.log(path);

  //filtros
  var f_filename = $("#file_name").attr("class");
  var f_size = $("#size").attr("class");
  var f_type = $("#type").attr("class");
  var f_modified = $("#modified").attr("class");

  var filtro = "text";
  var order = false; //false es ascendente
  if ($(".files-container table tr th.selected").length) {
    var identifier = $(".files-container table tr th.selected").attr("id");
    //asignamos a la variable filtro el valor del campo que usaremos para ordenar
    switch(identifier){
      case "file_name":
      filtro = "text";
      break;
      case "size":
      filtro = "size_org";
      break;
      case "type":
      filtro = "mime";
      break;
      case "modified":
      filtro = "modified_unix";
      break;
    }

    if ($(".files-container table tr th.selected").hasClass("desc")) {
      order = true;
    }

  }

  if (path) {
    $.ajax({
      url: "ftp/get_list_files",
      type: "POST",
      dataType: 'json',
      data: {
        op: 'list_files',
        url: path,
        field: filtro,
        order: order,
      },
      success: function(files) {
        //console.log(files);
        content.empty();
        //actualizamos los directorios teniendo en cuenta el path inicial
        $.ajax({
            url: "ftp/get_list_files",
            type: "POST",
            dataType: 'json',
            data: {
            op: 'list_dirs',
            url: path,
          },
          success: function(folders) {
            console.log(folders);

            $.ajax({
              url: "ftp/get_list_files",
              type: "POST",
              data: {
                op: 'scan_selected_dir',
                url: path,
              },
              success: function(data) {
                if ($(".folders li a.active").length) {
                  $(".folders li a.active").find("span").html(data);
                }
              }
            });
            
            //si existe alguna carpeta seleccionada diferente de la raiz, refrescamos unicamente esa carpeta
            if ($(".folders li a.active").length) {
                //region izquierda o folders
                var markup = "";
                if(folders) {
                console.log("1");
                  $.each( folders, function( i, val ) {

                    markup += "<li class='folder'><a href='#' data-rel='"+val.id+"' id='folder-"+i+"' class='no-child'>"+val.text;
                    if(val.new_files > 0) {
                      markup += "<span class='news'>"+val.new_files+"</span>";
                    }
                    markup += "</a></li>";
                    
                  });
                  if ($(".folders li a.active").siblings("ul").length) {
                    $(".folders li a.active").siblings("ul").empty().append(markup);
                  } else {
                    $(".folders li a.active").parent().append("<ul class='folders'>"+markup+"</ul>");
                  }
                }
            } else {
              console.log("2");
              //la carpeta seleccionada es la raiz, entonces refrescamos la carpeta raiz
              //region izquierda o folders
              var markup = "";
              if(folders) {
                $.each( folders, function( i, val ) {

                  markup += "<li class='folder'><a href='#' data-rel='"+val.id+"' id='folder-"+i+"' class='no-child'>"+val.text;
                  if(val.new_files > 0) {
                    markup += "<span class='news'>"+val.new_files+"</span>";
                  }
                  markup += "</a></li>";
                });
                $(".folders").empty().append(markup);
              }
            }
          }
        });
        if($.isEmptyObject(files)) {
              //si el directorio esta vacio
              content.empty();
              var markup = "<table><tr>";
              markup += "<th id='file_name' class='"+f_filename+"'>"+textos.generales[lenguaje].nombre_archivo+"</th>"; 
              markup += "<th id='size' class='"+f_size+"'>"+textos.generales[lenguaje].peso+"</th>"; 
              markup += "<th id='type' class='"+f_type+"'>"+textos.generales[lenguaje].tipo+"</th>"; 
              markup += "<th id='modified' class='"+f_modified+"'>"+textos.generales[lenguaje].creado+"</th>"; 
              markup += "<th id='actions'>"+textos.generales[lenguaje].acciones+"</th>"; 
              markup += "</tr></table>";
              markup += "<div class='empty-message'>"+textos.generales[lenguaje].directorio_vacio+"</div>";
              content.append(markup);
            } else {
              console.log(files);
              var right = "<table><tr>";
              right += "<th id='file_name' class='"+f_filename+"'>"+textos.generales[lenguaje].nombre_archivo+"</th>"; 
              right += "<th id='size' class='"+f_size+"'>"+textos.generales[lenguaje].peso+"</th>"; 
              right += "<th id='type' class='"+f_type+"'>"+textos.generales[lenguaje].tipo+"</th>"; 
              right += "<th id='modified' class='"+f_modified+"'>"+textos.generales[lenguaje].creado+"</th>"; 
              right += "<th id='actions'>"+textos.generales[lenguaje].acciones+"</th>"; 
              right += "</tr>";
              $.each( files, function( i, val ) {

                right += "<tr>";
                right += "<td>"+ val.img +"<a href='#' data-rel='"+val.id+"' id='files-"+i+"' class='no-child "+val.clase+" "+val.cls+"'>"+val.text+"</a></td>";
                if (val.size){
                 right += "<td><a href='#' data-rel='"+val.id+"' id='files-"+i+"' class='no-child "+val.clase+" "+val.cls+"'>"+val.size+"</a></td>";
               } else {
                 right += "<td></td>";
               }

               right += "<td><a href='#' data-rel='"+val.id+"' id='files-"+i+"' class='no-child "+val.clase+" "+val.cls+"'>"+val.mime+"</a></td>";
               right += "<td><a href='#' data-rel='"+val.id+"' id='files-"+i+"' class='no-child "+val.clase+" "+val.cls+"'>"+val.modified+"</a></td>";
                if (val.cls == "file") {
                    right += "<td>";
                    if(val.clase == 'image') {
                      right += "<a id='ver' href='#' data-rel='"+val.id+"' id='files-"+i+"' class='no-child "+val.clase+" "+val.cls+"'>ver</a>";
                    }
                    right += "<a id='descargar' href='#' data-rel='"+val.id+"' id='files-"+i+"' class='no-child "+val.clase+" "+val.cls+"'>Descargar</a></td>";
                } else {
                  right += "<td><a id='ver' href='#' data-rel='"+val.id+"' id='files-"+i+"' class='no-child "+val.clase+" "+val.cls+"'>ver</a></td>";
                }
               right += "</tr>";

             });
              content.append(right);

            }

             $(".overlay").hide();

          }
    });
  }
}

})(jQuery);