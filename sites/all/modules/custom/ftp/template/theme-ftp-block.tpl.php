<div id="block-ftp-ftp-block">
	<div class="ftp-container">
	<div class="header-ftp">
		<div class='options'>
			<div class='ftp-menu'>
				<span><?php print t("Menú"); ?></span>
				<?php 
				$menup = menu_navigation_links('main-menu');                      
                                foreach ($menup as $key => $menulink){                      
                                  $menup[$key]['title'] = t($menulink['title']);
                                }                      
                                print theme('links__main_menu', array('links' => $menup));
                                //Quienes somos y noticias
                                $menu = menu_navigation_links('menu-menu-principal-footer');
                  				print theme('links__menu_menu_principal_footer', array('links' => $menu));
				?>
			</div>
			<div class='language-block'>
				<?php 
				$search_form = block_load('locale', 'language_content');
				if (isset($search_form->bid)) {
					$block = _block_get_renderable_array(_block_render_blocks(array($search_form)));
					print render($block);
				}
				?>
			</div>
			<div class='zona-usuarios'>
				<?php 
				$block = module_invoke('zona_usuario', 'block_view', 'block_zona_usuario');
				print render($block['content']);
				?>
			</div>
		</div>
	</div>
	<div class="content-ftp">
		<div class="left">
			<div class='logo'>
				<a href="<?php print base_path().$lenguaje ?>"><img src="<?php print base_path().drupal_get_path('module', 'ftp').'/img/Logo_Caracol.png'; ?>"></a>
			</div>
			<ul class="folders">
				<?php foreach ($folders as $delta => $item) : ?>
				<li class="folder"><a href="#" data-rel="<?php print $item->id; ?>" id="folder-<?php print $delta; ?>" class="no-child"><?php print $item->text; ?>
					<?php if($item->new_files > 0): ?>
						<span class='news'><?php print $item->new_files; ?></span>
					<?php endif; ?>
				</a></li>
				<?php endforeach; ?>
			</ul>
		</div>
		<div class="right">
			<div class='options-left'>
				<div class='up-level'><a href="#" data-rel=""><?php print $textos->ftp->generales->$lenguaje->subir_nivel; ?></a></div>
			</div>
			<?php if ($access): ?>
			<div class='options-right'>
				<div id="create-folder" class="create-folder"><a href="#" data-rel=""><?php print $textos->ftp->generales->$lenguaje->crear_directorio; ?></a></div>
				<div id="upload-file" class="upload-file"><a href="#" data-rel=""><?php print $textos->ftp->generales->$lenguaje->subir_archivo; ?></a></div>
				<div id="renombrar" class="renombrar"><a href="#" data-rel=""><?php print $textos->ftp->generales->$lenguaje->renombrar; ?></a></div>
				<div id="eliminar" class="eliminar"><a href="#" data-rel=""><?php print $textos->ftp->generales->$lenguaje->eliminar; ?></a></div>
			</div>
		<?php endif; ?>
			<div class='files-container'>
				<table>
					<tr>
						<th id="file_name" class="selected asc"><?php print $textos->ftp->generales->$lenguaje->nombre_archivo; ?></th>
						<th id="size"><?php print $textos->ftp->generales->$lenguaje->peso; ?></th>
						<th id="type"><?php print $textos->ftp->generales->$lenguaje->tipo; ?></th>
						<th id="modified"><?php print $textos->ftp->generales->$lenguaje->creado; ?></th>
						<th id="actions"><?php print $textos->ftp->generales->$lenguaje->acciones; ?></th> 
					</tr>
					<?php foreach ($files as $delta => $item) : ?>
					<tr>
						<td><?php print $item->img; ?><a href='#' data-rel='<?php print $item->id; ?>' id='files-<?php print $delta; ?>' class='no-child <?php print $item->cls." ".$item->clase; ?>'><?php print $item->text; ?></a></td>
						<?php if (isset($item->size)){ ?>
						<td><a href='#' data-rel='<?php print $item->id; ?>' id='files-<?php print $delta; ?>' class='no-child <?php print $item->cls." ".$item->clase; ?>'><?php print $item->size; ?></a></td>
						<?php } else { ?>
						<td></td>
						<?php } ?>
						<td><a href='#' data-rel='<?php print $item->id; ?>' id='files-<?php print $delta; ?>' class='no-child <?php print $item->cls." ".$item->clase; ?>'><?php print $item->mime; ?></a></td>
						<td><a href='#' data-rel='<?php print $item->id; ?>' id='files-<?php print $delta; ?>' class='no-child <?php print $item->cls." ".$item->clase; ?>'><?php print $item->modified; ?></a></td>
						<?php if ($item->cls == 'file'){ ?>
						<td>
							<?php if($item->clase == 'image'): ?>
							<a  id="ver"  href='#' data-rel='<?php print $item->id; ?>' id='files-<?php print $delta; ?>' class='no-child <?php print $item->cls." ".$item->clase; ?>'>ver</a>
							<?php endif; ?>
							<a  id="descargar"  href='#' data-rel='<?php print $item->id; ?>' id='files-<?php print $delta; ?>' class='no-child <?php print $item->cls." ".$item->clase; ?>'>Descargar</a>
						</td>
						<?php } else { ?>
						<td><a  id="ver"  href='#' data-rel='<?php print $item->id; ?>' id='files-<?php print $delta; ?>' class='no-child <?php print $item->cls." ".$item->clase; ?>'>ver</a></td>
						<?php } ?>
					</tr>
					<?php endforeach; ?>
			</table>
			</div>
		</div>
	</div>
	<div class="overlay"></div>
	<div class="loader"></div>
	<input type="hidden" id="input_path">
</div>
<div id="pop-up">
	<div class='bg-popup'></div>
	<div class='content-popup'>
		<div class="wrapper">
			<div class="close"></div>
			<div class='content'></div>
		</div>
	</div>
</div>
</div>