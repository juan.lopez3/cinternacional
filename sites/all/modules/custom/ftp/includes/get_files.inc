<?php



function ftp_get_files() {

  header("Content-type: text/html");
  header("Expires: Wed, 29 Jan 1975 04:15:00 GMT");
  header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
  header("Cache-Control: no-cache, must-revalidate");
  header("Pragma: no-cache");

	$root_folder = DRUPAL_ROOT;

	$operation = (isset($_REQUEST['op']))? $_REQUEST['op'] : 'list_files' ;

	if(isset($operation)) {

		try {

			switch ($operation) {
				case 'list_dirs':
					//obtenemos subdirectorios de la ubicacion enviada
					ftp_get_subdirs();
				break;
        case 'scan_selected_dir':
          //obtenemos subdirectorios de la ubicacion enviada
          ftp_scan_selected_dir();
        break;
				case 'list_files':
					ftp_list_files();
				break;
        case 'create_folder':
          ftp_create_folder();
        break;
        case 'upload_file':
          ftp_file_upload();
        break;
        case 'download_file':
          ftp_file_download();
        break;
        case 'rename':
          ftp_change_name();
        break;
        case 'delete':
          ftp_file_delete();
        break;
			}

			
		} catch (Exception $e) {
			ftp_error($e);	
		}

	} else {
		ftp_error();
	}

	die();
}

/**
* Lista de subdirectorios 
*/
function ftp_get_subdirs(){
	
	//asignamos la url a consultar enviada por post desde ajax, dejamos por defecto el folder inicial
	$url = (isset($_REQUEST['url']) && !empty($_REQUEST['url']))? $_REQUEST['url'] : DRUPAL_ROOT."".DEFAULT_URL;

	_ftp_scan_dir($url, $subdirs, $files);
  //ordenamos por defecto nombre de manera ascendente
  $subdirs = record_sort($subdirs, "text");
	echo json_encode($subdirs);
}

/**
* Escaneamos el folder actualmente seleccionado
*/
function ftp_scan_selected_dir(){
  
  //asignamos la url a consultar enviada por post desde ajax, dejamos por defecto el folder inicial
  $url = (isset($_REQUEST['url']) && !empty($_REQUEST['url']))? $_REQUEST['url'] : DRUPAL_ROOT."".DEFAULT_URL;

  $items = _ftp_get_new_items($url);
  //ordenamos por defecto nombre de manera ascendente
  echo $items;
}

/**
* Lista de archivos 
*/

function ftp_list_files() {
  	//asignamos la url a consultar enviada por post desde ajax, dejamos por defecto el folder inicial
	$url = (isset($_REQUEST['url']) && !empty($_REQUEST['url']))? $_REQUEST['url'] : DRUPAL_ROOT."".DEFAULT_URL;

	_ftp_scan_dir($url, $subdirs, $files);
	$files = array_merge($subdirs, $files);

  //ordenamiento
  $field = (isset($_REQUEST['field']))?$_REQUEST['field'] : "text";
  $order = (isset($_REQUEST['order']) && $_REQUEST['order'] == "true" )? true : false;

  $files = record_sort($files, $field, $order);
	echo json_encode($files);
}

/**
* Creacion de subdirectorio
*/
function ftp_create_folder() {
  
  $path = $_REQUEST['url'];
  $name = $_REQUEST['name'];

  $dir = $path . DIRECTORY_SEPARATOR . $name;

  if (file_exists($dir)) {
    echo 'exists';
  }
  else {
    mkdir($dir, 0755);
    echo 'success';
  }
}

/**
* Subida de archivos
*/

function ftp_file_upload() {
  
  $parent = $_REQUEST['url'];

  $success = true;
  $failed = array();
  $succeded = array();
  foreach ($_FILES as $key => &$file) {
    $target_path = $parent . DIRECTORY_SEPARATOR . basename($file['name']);
    $partial = move_uploaded_file($file['tmp_name'], $target_path);
    if ($partial) {
      $succeded[] = $file['name'];
    }
    else {
      $failed[] = $file['name'];
    }
    $success = $success && $partial;
  }
  
  $response = new stdClass();
  $response->success = "true";
  $response->succeded = $succeded;
  $response->failed = $failed;
  echo json_encode($response);
}

/*
Descargar archivo
*/
function ftp_file_download() {
  $file = $_REQUEST['file'];

  if (file_exists($file)) {
    header('Content-Description: File Transfer');
    header('Content-Type: ' . file_get_mimetype($file));
    header('Content-Disposition: attachment; filename=' . basename($file));
    header('Content-Transfer-Encoding: binary');
    header('Expires: 0');
    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
    header('Pragma: public');
    header('Content-Length: ' . filesize($file));
    ob_clean();
    flush();
    readfile($file);
    die();
  }
  else {
    echo 'Invalid File name.';
  }
}

function ftp_change_name() {
  
  $path = $_REQUEST['path'];
  $new = $_REQUEST['name'];

  $path_parts = pathinfo($path);

  //agregamos la extension
  $new = dirname($path) . DIRECTORY_SEPARATOR . $new;
  $new .= ".".$path_parts['extension'];


  $success = rename($path, $new);
  if (!$success) {
    echo 'Operation failed';
  }
}

function ftp_file_delete() {
  if (isset($_REQUEST['nodes'])) {
    
    $path = $_REQUEST['nodes'];
    //$nodes = json_decode($nodes);

    if (is_file($path)) {
      unlink($path);
    }
    else {
      if (!is_dir($path)) {
        die('Error: File not found');
      }
      ftp_file_delete_directory($path);
    }

  }
  
}

function ftp_file_delete_directory($dir) {
  if (!file_exists($dir))
    return true;
  if (!is_dir($dir))
    return unlink($dir);
  foreach (scandir($dir) as $item) {
    if ($item == '.' || $item == '..')
      continue;
    if (!ftp_file_delete_directory($dir . DIRECTORY_SEPARATOR . $item))
      return false;
  }
  return rmdir($dir);
}

//handlers folders and files default 
function ftp_default_get_subdirs(){

  $url = DRUPAL_ROOT."".DEFAULT_URL; 


  _ftp_scan_dir($url, $subdirs, $files);
  //ordenamos por defecto nombre de manera ascendente
  $subdirs = record_sort($subdirs, "text");
  return json_encode($subdirs);
}

function ftp_default_list_files(){
  
  $url = DRUPAL_ROOT."".DEFAULT_URL; 

  _ftp_scan_dir($url, $subdirs, $files);
  $files = array_merge($subdirs, $files);
  $files = record_sort($files, "text");
  return json_encode($files);
}

/**
* Buscamos archivos y directorios dependiendo del criterio de busqueda, el parametro
*/
function _ftp_scan_dir($base, &$subdirs, &$files) {


  global $user;
  $user_info = user_load($user->uid);

  //cargamos la info del usuario para obtener las producciones relacionadas al mismo
  $nids = array();
  if(!empty($user_info->field_user_producciones)) {
    foreach ($user_info->field_user_producciones[LANGUAGE_NONE] as $key => $value) {
      $nids[] = $value['target_id'];
    }
  }
  
  //informacion de los nodos
  $info_nodes = entity_load('node', $nids);

  //recorremos el array de nodos para obtener unicamente el nombre de la carpeta de la produccion
  $folders = array();
  foreach ($info_nodes as $key => $value) {

    if(!empty($value->field_nombre_carpeta)) {
      $folders[] = $value->field_nombre_carpeta[LANGUAGE_NONE][0]['value'];
    }
  }

  //url de acceso al folder principal
  $base_url = DRUPAL_ROOT."".DEFAULT_URL;
  //kpr($folders);

  $subdirs = array();
  $files = array();

  //kpr($base);

  if (is_dir($base)) {
    $d = dir($base);
    while ($f = $d->read()) {
      if ($f == '.' || $f == '..') // Uncomment this if system files are not to be served || substr($f, 0, 1) == '.')
        continue;

      $filename = $base . '/' . $f;
      $lastmod = date('M j, Y, g:i a', filemtime($filename));

      if (is_dir($filename)) {

        if (in_array($f, $folders) || $base_url != $base) {
          $qtip = 'Type: Folder<br />Last Modified: ' . $lastmod;
          $img = '';
          $clase = '';

          //contador de nuevos archivos
          $folder = dir($filename);
          $contador = 0;

          while ($a = $folder->read()) {
            if ($a == '.' || $a == '..')
            continue;

            $name = $filename . '/' . $a;
            $file_date = filemtime($name);

            $fecha = new DateTime();
            $fecha->modify("-30 days");

            if ($file_date > $fecha->getTimestamp()) {
              $contador++;
            }

          }

          $fecha = new DateTime();
          $fecha->modify("-30 days");

          if (filemtime($filename) > $fecha->getTimestamp()) {
              $clase .= ' new-content';
          }

          //imagen de folder por defecto
          $path = drupal_get_path('module', 'ftp')."/img/carpeta_big.png";

          $img = "<div class='image-container ".$clase."'><img src='".base_path().$path."' class='folder-img' /></div>";
          
          $subdirs[] = array(
              'id' => $filename,
              'img' => $img,
              'clase' => $clase,
              'text' => $f,
              'cls' => 'folder',
              'modified' => $lastmod,
              'modified_unix' => filemtime($filename),
              'owner' => fileowner($filename),
              'perm' => substr(decoct(fileperms($filename)), 1),
              'mime' => 'folder',
              'size_org' => '',
              'new_files' => $contador,
              'qtip' => $qtip);

        }
      }
      else {
        $mime = file_get_mimetype($filename);

        $img = '';
        $clase = '';

        //imagen de folder por defecto
        $path = drupal_get_path('module', 'ftp')."/img/docs.png";

        $fecha = new DateTime();
        $fecha->modify("-30 days");

        if (filemtime($filename) > $fecha->getTimestamp()) {
          $clase .= ' new-content';
        }

        $img = "<div class='image-container ".$clase."'><img src='".base_path().$path."' class='file-img' /></div>";

        if(strpos($mime, 'image') !== false) {
          //print_r($filename);
          $pos = strpos($filename, '/ftp');
          $rest = substr($filename, $pos);

          $vars = array(
            'style_name' => 'ftp_thumbnail',
            'path' => 'public://'.$rest,
            'width' => '100%',
            'height' => '100%',
            'attributes' => array('class' => 'some-img', 'id' => 'my-img'),
            );

          $img = theme_image_style($vars);
          $clase = 'image';

          $fecha = new DateTime();
          $fecha->modify("-30 days");

          if (filemtime($filename) > $fecha->getTimestamp()) {
            $clase .= ' new-content';
          }

          $img = "<div class='image-container ".$clase."'>".$img."</div>";
        }


        $files[] = array(
            'id' => $filename,
            'img' => $img,
            'clase' => $clase,
            'text' => $f,
            'cls' => 'file',
            'modified' => $lastmod,
            'modified_unix' => filemtime($filename),
            'owner' => fileowner($filename),
            'perm' => substr(decoct(fileperms($filename)), 2),
            'mime' => file_get_mimetype($filename),
            'size_org' => filesize($filename),
            'size' => ftp_format_bytes(filesize($filename), 2));
      }
    }

    $d->close();

  }
  else {
    ftp_error();
  }
}


function _ftp_get_new_items($base) {


  //url de acceso al folder principal
  $base_url = DRUPAL_ROOT."".DEFAULT_URL;
  $contador = 0;

  if (is_dir($base)) {
    $d = dir($base);

    while ($f = $d->read()) {
      if ($f == '.' || $f == '..') // Uncomment this if system files are not to be served || substr($f, 0, 1) == '.')
        continue;

      $filename = $base . '/' . $f;
      $lastmod = date('M j, Y, g:i a', filemtime($filename));

      $file_date = filemtime($filename);

      $fecha = new DateTime();
      $fecha->modify("-30 days");

      if ($file_date > $fecha->getTimestamp()) {
        $contador++;
      }

    }

    $d->close();

  }
  else {
    ftp_error();
  }

  return $contador;
}


/**
* formato para el tamaño del archivo
*/
function ftp_format_bytes($val, $digits = 3, $mode = 'SI', $bB = 'B') { //$mode == 'SI'|'IEC', $bB == 'b'|'B'
  $si = array('', 'K', 'M', 'G', 'T', 'P', 'E', 'Z', 'Y');
  $iec = array('', 'Ki', 'Mi', 'Gi', 'Ti', 'Pi', 'Ei', 'Zi', 'Yi');
  switch (strtoupper($mode)) {
    case 'SI':
      $factor = 1000;
      $symbols = $si;
      break;
    case 'IEC':
      $factor = 1024;
      $symbols = $iec;
      break;
    default:
      $factor = 1000;
      $symbols = $si;
      break;
  }
  switch ($bB) {
    case 'b':
      $val *= 8;
      break;
    default:
      $bB = 'B';
      break;
  }
  for ($i = 0; $i < count($symbols) - 1 && $val >= $factor; $i++)
    $val /= $factor;
  $p = strpos($val, '.');
  if ($p !== false && $p > $digits)
    $val = round($val);
  elseif ($p !== false)
    $val = round($val, $digits - $p);
  return round($val, $digits) . ' ' . $symbols[$i] . $bB;
}

/**
* metodo para imprimir mensajes de error
*/
function ftp_error( $msg = 'Error al realizar la petición.' ) {
	die($msg);
}

function record_sort($records, $field, $reverse=false)
{
    $hash = array();

    foreach($records as $key => $record)
    {

        $hash[$record[$field].$key] = $record;
    }
    
    ($reverse)? krsort($hash) : ksort($hash);
    
    $records = array();
    
    foreach($hash as $record)
    {
        $records []= $record;
    }
    
    return $records;
}