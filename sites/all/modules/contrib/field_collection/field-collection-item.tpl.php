<?php

/**
 * @file
 * Default theme implementation for field collection items.
 *
 * Available variables:
 * - $content: An array of comment items. Use render($content) to print them all, or
 *   print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $title: The (sanitized) field collection item label.
 * - $url: Direct url of the current entity if specified.
 * - $page: Flag for the full page state.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. By default the following classes are available, where
 *   the parts enclosed by {} are replaced by the appropriate values:
 *   - entity-field-collection-item
 *   - field-collection-item-{field_name}
 *
 * Other variables:
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 *
 * @see template_preprocess()
 * @see template_preprocess_entity()
 * @see template_process()
 */
?>
<div class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  <div class="content"<?php print $content_attributes; ?>>
    <?php
	//print get_object_vars($content);
	//print '<pre>';
	 //var_dump($content);
	//print '</pre>';
	$language = $content['field_idioma_doblado']['#object']->field_idioma_doblado['und'][0]['entity']->title;
	$id = $content['field_idioma_doblado']['#object']->field_produccion_doblada['und'][0]['node']->vid;
	$title = addslashes($content['field_idioma_doblado']['#object']->field_produccion_doblada['und'][0]['node']->title);

	$filename = $content['field_idioma_doblado']['#object']->field_idioma_doblado['und'][0]['entity']->field_bandera['und'][0]['filename'];
        $url_file_image = file_create_url(file_build_uri("{$filename}"));

	$languages = language_list();
        $url_es = url('node/'. $id, ['language' => $languages['es']]);
        $url_en = url('node/'. $id, ['language' => $languages['en']]);

        print '{"idioma":"'.$language.'", "id":"'.$id.'", "title":"'.$title.'", "image":"'.$url_file_image.'", "url_es":"'.$url_es.'", "url_en":"'.$url_en.'"}';

	//print json_encode($result, JSON_UNESCAPED_UNICODE);
	//print render($content);
    ?>
  </div>
</div>
